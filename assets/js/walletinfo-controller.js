var app = angular.module('myApp');

app.controller('WalletInfoController',['$scope', 'walletService', 'currencyPriceService', '$http', function($scope, walletService, currencyPriceService, $http){

    $scope.walletInformation = {bitcoins:0};

    $scope.userData = {};


    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load UserController route current: '+$currentRoute);

        $scope.getCurrentDeposit();
        $scope.getBtcAddress();
        $scope.getBtcBalance();
    });

    $scope.loadUserData = function () {

        var result = 'NoResult';

        $http.post('/Api/User/getUserData',$.param( {data:$scope.user.userEmail} )).then(function(myReponseData) {
            $scope.userData = myReponseData.data[0];

            console.log($scope.userData);
        });
    }

}]);