var app = angular.module('myApp');

app.controller('BitcoinController',['$scope', 'dataService', function($scope, dataService){

    $scope.dataService = dataService;

    $scope.walletAddress = 'unknown';

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load ProfileController route current: '+$currentRoute);

        $scope.loadWalletInformation();
    });

    $scope.loadWalletInformation = function (dataService) {

        var result = 'NoResult';

        console.log('Load loadBtcAddress');

        $scope.dataService.getData('/api/bitcoin/getWalletInformation').then(function(myReponseData) {
            $scope.walletInformation = JSON.parse(myReponseData.data);

            console.log(result);
            console.log($scope.walletInformation);
        });

    }

    $scope.generateBitcoinAddress = function () {

        $scope.dataService.getData('/api/bitcoin/generateAdress').then(function(myResponseData){

            $scope.walletAddress = JSON.parse(myResponseData.data);

            console.log($scope.walletAddress);
        });

    }

}]);