var app = angular.module('myApp');

app.controller('depositController',['$scope', 'dataService','$http', '$window', function($scope, dataService, $http, $window){

    $scope.dataService = dataService;

    $scope.number = '4242424242424242';
    $scope.expiry = '10/23';
    $scope.cvc = '999';
    $scope.data = {
        dataamount: '0.00',
        currency: 'EUR',
        datadescription: 'Deposit Crypto market',
        datalocale: 'auto',
        datakey: 'pk_test_8gRcUWoVrhS5NdSSYOlC0DJg',
        stripeToken: 'pk_test_8gRcUWoVrhS5NdSSYOlC0DJg',
    };

    $scope.paymentmethod = 'card';

    $scope.sourceData = '';

    $scope.chargeResponse = '';

    $scope.status = '';

    $scope.$on("depositController", function($currentRoute, $previousRoute) {

        console.log('Load depositController route current: '+$currentRoute);
    });

    $scope.amountChanged = function(){

        // Because Stripe JS expect two decimal
        if($scope.data.dataamount.indexOf('.') === -1)
        {
            $scope.data.dataamount = $scope.data.dataamount+'.00';
        }
    }


    // Stripe Response Handler
    $scope.stripeCallback = function (code, result) {
        if (result.error) {

            $scope.status = 'Charging failed: '+ result.error.message;

        } else {
            $scope.status = 'Charging credit card...';
            $scope.isChargingOk = true;
            $scope.data.stripeToken =  result.id;
            $scope.depositNow();
        }
    };

    $scope.depositNow = function () {

        console.log('depositNow');

        $http.post('/Api/Ledger/deposit',$.param($scope.data)).then(function(myReponseData) {

            console.log('result');
            console.log(myReponseData);

            $scope.chargeResponse = myReponseData.data;
            $scope.status = myReponseData.data.status;

            $scope.status = 'Done! Thank you for deposit your account.';
        });
    }

    $scope.sofortPayment = function(){

        $scope.data.method = 'sofortPayment';

        $http.post('/api/index.php',$.param($scope.data)).then(function(myReponseData) {

            console.log('result:sofortPayment');
            console.log(myReponseData);

            $scope.sourceData = myReponseData.data;

            console.log($scope.sourceData);
            console.log('URL' +$scope.sourceData.redirect.url);
            //
            // $scope.chargeResponse = myReponseData.data;
            // $scope.status = myReponseData.data.status;
            //
            // $scope.status = 'Done! Thank you for deposit your account.';
            $window.open($scope.sourceData.redirect.url, '_blank');
        });
    }

}]);