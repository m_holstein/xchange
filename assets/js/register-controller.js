
var app = angular.module('myApp');

app.controller('RegisterController',['$scope', '$http', 'captchaService', function($scope, $http, captchaService){

    $scope.expectedFillDuration = 8;

    $scope.newUser = {
        method: 'register',
        username: '',
        useremail: '',
        userpassword: ''
    };

    // Set the default value of inputType
    $scope.inputType = 'password';

    // Hide & show password function
    $scope.hideShowPassword = function(){
        if ($scope.inputType == 'password')
            $scope.inputType = 'text';
        else
            $scope.inputType = 'password';
    };

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load RegisterController route current: '+$currentRoute);

        captchaService.captchaStart();
    });

    $scope.isUsernameAvailable = function () {

        var data =  {method:'register',
            data: 'blah'
        };

        $http.post('/api/User/isUsernameAvailable',$.param($scope.newUser));
    }

    $scope.register = function () {

        console.log('Register');

        captchaService.captchaStop();

        if(captchaService.getFormFillDuration() > $scope.expectedFillDuration){

            $http.post('/api/User/registerNewUser',$.param($scope.newUser));
        } else {

            console.log('Too fast ' + captchaService.getFormFillDuration());
        }

        console.log( captchaService.getFormFillDuration());
    }

}]);