var app = angular.module('myApp');

app.controller('UserController',['$scope', '$routeParams', 'dataService', '$http', function($scope, $routeParams, dataService, $http){

    $scope.dataService = dataService;

    $scope.walletInformation = {bitcoins:0};

    $scope.userData = {};

    $scope.email = $routeParams.email;

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load UserController route current: '+$currentRoute);

        $scope.loadUserData();
    });

    $scope.loadUserData = function () {

        var result = 'NoResult';

        $http.post('/Api/User/getUserData',$.param( {data:$scope.user.userEmail} )).then(function(myReponseData) {
            $scope.userData = myReponseData.data[0];

            console.log($scope.userData);
        });
    }

    $scope.getBtcBalance = function(){

        console.log('getBtcBalance');

        var data = {currency:'Bitcoin',
                    user: $scope.user
                    }

        $http.post('/Api/UserWallet/getBalance',$.param( data )).then(function(myReponseData) {

            console.log('result getBtcBalance');
            console.log(myReponseData);

            $scope.walletInformation.bitcoins = myReponseData.data;

        });

    }
}]);