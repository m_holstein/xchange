var app = angular.module('myApp');

app.controller('UserActivateController',['$scope', '$routeParams', '$http', function($scope, $routeParams, $http){

    $scope.status = '';

    $scope.data = {
        method: 'acitvateuser',
        uuid: $routeParams.activateuid
    };

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load UserActivateController route current: '+$currentRoute);

        $scope.activateUser();
    });

    $scope.activateUser = function () {

        $http.post('/Api/User/activateUserAccount',$.param($scope.data)).then(function(myReponseData) {

            console.log('result');
            console.log(myReponseData);

            $scope.status = myReponseData.data;

        });
    }
}]);