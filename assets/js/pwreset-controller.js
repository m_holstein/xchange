
var app = angular.module('myApp');

app.controller('PwresetController',['$scope', 'dataService','$http', '$routeParams', function($scope, dataService, $http, $routeParams){

    $scope.dataService = dataService;

    $scope.status = '';

    $scope.data = {
        method: 'resetPassword',
        resettoken:  $routeParams.resettoken,
        password: ''
    };

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load PwresetController route current: '+$currentRoute);
    });

    $scope.setNewPassword = function () {

        console.log('setNewPassword');

        $scope.status = 'Save new password';

        $http.post('/api/User/resetPassword',$.param($scope.data)).then(function(myReponseData) {

            $scope.status = 'Done!';

            console.log('result setNewPassword');
            console.log(myReponseData);

            $scope.status = myReponseData.data;

        });
    }

}]);