var app = angular.module('myApp');

app.controller('LogoutController',['$scope', 'dataService','$http', '$route', function($scope, dataService, $http, $route){

    $scope.dataService = dataService;

    $scope.data = {
        method: 'logout',
    };

    $scope.status = 'Please wait...';

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load LogoutController route current: '+$currentRoute);

        $scope.logout();
    });



}]);