var app = angular.module('myApp');

app.controller('SendbitcoinsController',['$scope', 'dataService','$http', function($scope, dataService, $http){

    $scope.dataService = dataService;

    $scope.data = {
        amountsend: '',
        receiveraddress: '',
        receivernote: '',
    };

    $scope.status = '';

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load SendbitcoinsController route current: '+$currentRoute);
    });

    $scope.sendCoins = function () {

        console.log('SendCoins');

        $http.post('/Api/UserWallet/sendBitcoins',$.param($scope.data)).then(function(myReponseData) {

            console.log('result SendCoins');
            console.log(myReponseData);

            $scope.status = myReponseData.data;

        });
    }

}]);