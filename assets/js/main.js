require('jquery')
require('angular');
require('angular-route');
require('angular-material');
require('angular-loading-bar');

require('./components/ng-caps-lock/ng-caps-lock.min');
require('./components/angular-payments');

var app = angular.module("myApp", ['angular-loading-bar', "ngRoute", "angularPayments", 'ngMaterial', 'ngCapsLock']);

app.config(['$locationProvider','$httpProvider', '$mdThemingProvider', function($locationProvider, $httpProvider, $mdThemingProvider) {
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode(true);
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    // $mdThemingProvider.generateThemesOnDemand(true);
}]);

app.run(function($window) {
    $window.Stripe.setPublishableKey('pk_test_8gRcUWoVrhS5NdSSYOlC0DJg');
});

app.config(function($routeProvider) {
    $routeProvider
        .when("/activate/:activateuid", {
            templateUrl : "/activateuser.html",
        })
        .when("/buybitcoins", {
            templateUrl : "/buybitcoins.html",
        })
        .when("/payed", {
            templateUrl : "/payed.html",
        })
        .when("/deposit", {
            templateUrl : "/deposit.html",
        })
        .when("/login", {
            templateUrl : "/login.html",
        })
        .when("/logout", {
            templateUrl : "/logout.html",
        })
        .when("/register", {
            templateUrl : "/register.html",
        })
        .when("/pwreset/:resettoken", {
            templateUrl : "/pwreset.html",
        })
        // .when("/getmarketproducts", {
        //     templateUrl : "/marketproducts.html",
        // })
        .when("/sendbitcoins", {
            templateUrl : "/sendbitcoins.html",
        })
        .when("/bitcoin/getWalletInformation", {
            templateUrl : "/walletInfo.html",
        })
        .when("/api/user/data/:email", {
            templateUrl : "/user.html",
        })
        .when("/", {
            templateUrl : "/main.html"
        })
        // .otherwise({ redirectTo: '/' })
    ;
});


app.service('dataService', function($http) {
    delete $http.defaults.headers.common['X-Requested-With'];

    this.getData = function(path) {
        // $http() returns a $promise that we can add handlers with .then()
        return $http({
            method: 'GET',
            url: path
        });
    }
});

app.service('currencyPriceService', function($http) {
    delete $http.defaults.headers.common['X-Requested-With'];

    this.getBtcPrice = function() {

        // $http() returns a $promise that we can add handlers with .then()
       return $http.post('/Api/CoinMarket/getCurrentBtcPrice').then(function(myReponseData) {

            return myReponseData.data;

        });

    }
});

app.service('captchaService', function($rootScope) {

    this.captchaStart = function() {

        $rootScope.formFillStart = Date.now() / 1000;

    }

    this.captchaStop = function() {

        $rootScope.formFillStop = Date.now()  / 1000;
        $rootScope.formFillDuration = $rootScope.formFillStop - $rootScope.formFillStart;

    }

    this.getFormFillDuration = function () {

        return $rootScope.formFillStop - $rootScope.formFillStart;
    }
});

app.controller('ProfileController',['$scope', 'dataService', function($scope, dataService){

    $scope.dataService = dataService;

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load ProfileController route current: '+$currentRoute);
        console.log('Load ProfileController route previous: '+$previousRoute);
        $scope.loadWallet();
    });

    $scope.loadWallet = function (dataService) {

        console.log('Load wallet');

        $scope.data = null;

        $scope.dataService.getData(function(dataResponse) {
            $scope.data = dataResponse;
        });

        console.log($scope.data);
    }
}]);

require('./components/wallet-Service');

require('./pwreset-controller.js');
require('./useractivate-controller.js');
require('./user-controller.js');
require('./bitcoin-controller.js');
require('./deposit-controller.js');
require('./sendbitcoins-controller.js');
require('./buybitcoins-controller.js');
require('./logout-controller.js');
require('./login-controller.js');
require('./main-controller.js');
require('./register-controller.js');
require('./walletinfo-controller');
