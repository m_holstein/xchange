var app = angular.module('myApp');

app.service('walletService', function($http) {
    delete $http.defaults.headers.common['X-Requested-With'];

    this.getBtcAddress = function(){

        console.log('getBtcAddress');

        return $http.get('/api/bitcoin/getBitCoinAddress').then(function(myReponseData) {

            console.log(myReponseData);

            return  myReponseData.data[0];
        });
    }

    this.getCurrentDeposit = function(){

        console.log('getCurrentDeposit');

        return $http.get('/api/user/getcurrentdeposit').then(function(myReponseData) {

            console.log(myReponseData);

            return  myReponseData.data;
        });
    }

    this.getBtcBalance = function(){

        console.log('getBtcBalance');

        var data = {currency:'Bitcoin'}

        return $http.post('/Api/UserWallet/getBalance',$.param( data )).then(function(myReponseData) {

            console.log('result getBtcBalance');
            console.log(myReponseData);

            return myReponseData.data;

        });

    }
});