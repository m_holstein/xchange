var app = angular.module('myApp');

app.controller('MainController',['$scope', 'walletService', 'currencyPriceService', '$http', '$mdSidenav', function($scope, walletService, currencyPriceService, $http, $mdSidenav){

    $scope.loadStatus = {
        isLoading: true
    };

    $scope.fromCurrency = 0.00;
    $scope.toCurrency = 0.00;
    $scope.convertResult = 0.00;

    $scope.currentCryptoPrice = '0.00€';

    $scope.currencyInfo = {
        currentBitcoinPrice: '--'
    }

    $scope.user = {loggedIn:false};

    $scope.currentNavItem = 'main';

    $scope.walletInfo = {
        currentDeposit: '--',
        btcWalletAddress: '--'
    }

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log($previousRoute);
        console.log('Load MainController route previous: '+$previousRoute);
        console.log('Load MainController route current: '+$currentRoute);

        $scope.refreshCurrencyCurse();

        $scope.checkSession();

    });


    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

    function buildToggler(componentId) {
        return function() {
            $mdSidenav(componentId).toggle();
        };
    }

    $scope.closeLeftMenu = function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav('left').close()
            .then(function () {
                console.log("close LEFT is done");
            });

    };

    $scope.getCurrentBtcPrice = function () {

        console.log('getCurrentBtcPrice');

        currencyPriceService.getBtcPrice().then(function(myReponseData) {

            console.log(myReponseData);

            $scope.currencyInfo.currentBitcoinPrice = myReponseData.price;

            console.log('result');
            console.log($scope.currencyInfo.currentBitcoinPrice);
            $scope.loadStatus.isLoading = false;
        });
    };

    $scope.getBtcAddress = function(){

        walletService.getBtcAddress().then(function(myReponseData) {

            console.log(myReponseData);

            $scope.walletInfo.btcWalletAddress =  myReponseData;

            console.log('result');
            console.log($scope.walletInfo.btcWalletAddress);
        });
    }

    $scope.getCurrentDeposit = function(){

        walletService.getCurrentDeposit().then(function(myReponseData) {

            console.log(myReponseData);

            $scope.walletInfo.currentDeposit =  myReponseData;

            console.log('result');
            console.log($scope.walletInfo.currentDeposit);
        });
    }

    $scope.getBtcBalance = function(){

        walletService.getBtcBalance().then(function(myReponseData) {

            $scope.walletInfo.BtcBalance = myReponseData;

        });
    }

    $scope.checkSession = function () {

        $http.post('/api/index.php',$.param({method:'checkSession'})).then(function(myReponseData) {

            console.log('resultcheckSession');
            console.log(myReponseData);

            if (myReponseData.data === 'loggedIn'){
                $scope.user.loggedIn = true;
            } else {

                $scope.user.loggedIn = false;
            }

            // if($scope.status === 'true'){
            //
            //     $scope.status = 'Redirect to main page...';
            //     // window.location = "/";
            //     $scope.user.loggedIn = true;
            //
            // } else {
            //
            //     $scope.status = 'Login failed. Wrong user data.';
            // }

        });
    }

    $scope.logout = function () {

        console.log('Logout');

        $http.post('/api/index.php',$.param( {method: 'logout'})).then(function(myReponseData) {

            console.log('result');
            console.log(myReponseData);

            if( myReponseData.data === 'true'){

                $scope.user.loggedIn = false;
            }

        });
    }

    $scope.loadWallet = function () {

        console.log('Load wallet');
    };

    $scope.refreshCurrencyCurse = function (){

        currencyPriceService.getBtcPrice().then(function(myReponseData) {

            console.log(myReponseData);

            $scope.currentCryptoPrice =  myReponseData.price
            $scope.toCurrency =  $scope.currentCryptoPrice

            console.log('getBtcPriceresult');
            console.log($scope.currentCryptoPrice);
        });
    }




}]);

