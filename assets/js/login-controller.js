var app = angular.module('myApp');

app.controller('LoginController',['$scope', 'captchaService','$http', '$location', function($scope, captchaService, $http, $location){

    captchaService.captchaStart();

    $scope.expectedFillDuration = 8;

    $scope.pwreset = false;
    $scope.pwresetText = 'Forgot password';

    $scope.data = {
        useremail: '',
        userpassword: ''
    };

    $scope.resetData = {
        useremail: ''}

    $scope.status = '';

    // Set the default value of inputType
    $scope.inputType = 'password';

    // Hide & show password function
    $scope.hideShowPassword = function(){
        if ($scope.inputType == 'password')
            $scope.inputType = 'text';
        else
            $scope.inputType = 'password';
    };

    $scope.login = function () {

        console.log('Login');

        $scope.status = 'Check data...';

        captchaService.captchaStop();

        if(captchaService.getFormFillDuration() > $scope.expectedFillDuration){

            $http.post('/api/User/login',$.param($scope.data)).then(function(myReponseData) {

                console.log('result');
                console.log(myReponseData);

                $scope.status = myReponseData.data;

                if($scope.status === 'true'){

                    $scope.status = '';
                    $scope.user.loggedIn = true;
                    $location.url('/');

                } else {

                    $scope.status = 'Login failed. Wrong user data.';
                }

            });

        } else {

            console.log('Too fast ' + captchaService.getFormFillDuration());
        }
    }

    $scope.forgotPassword = function () {

        console.log('pwForgot');

        if ($scope.pwreset === false){

            $scope.status = 'Please enter your E-Mail address to receive your reset link.';
            $scope.pwreset = true;
            $scope.pwresetText = 'Login';

        } else {
            $scope.status = '';
            $scope.pwresetText = 'Forgot password';
            $scope.pwreset = false;
        }
    }

    $scope.orderPwResetLink = function () {

        $scope.status = 'Sending E-Mail...';

        console.log('orderPwResetLink');

        $http.post('/api/User/sendPwResetMail',$.param($scope.resetData)).then(function(myReponseData) {

            console.log('result');
            console.log(myReponseData);

            $scope.status = myReponseData.data;

        });
    }

}]);