var app = angular.module('myApp');

app.controller('buybitcoinsController',['$scope', 'walletService','$http', 'currencyPriceService', function($scope, walletService, $http, currencyPriceService){

    $scope.status = '';

    $scope.currentDeposit = '--';

    $scope.data = {
        amountsbuy: 0
    };

    $scope.$on("$routeChangeSuccess", function($currentRoute, $previousRoute) {

        console.log('Load buybitcoinsController route current: '+$currentRoute);

        $scope.getCurrentBtcPrice();
        $scope.getCurrentDeposit();
        $scope.getBtcAddress();
    });


    $scope.buyBitcoins = function () {

        $http.post('/api/CoinMarket/buyBitCoins',$.param($scope.data)).then(function(myReponseData) {

            console.log('result');
            console.log(myReponseData);

            $scope.buyResponse = myReponseData.data;
            $scope.status = myReponseData.data.status;

        });
    }

    $scope.orderBitcoins = function () {

        $http.post('/api/CoinMarket/exchangeToBitCoin',$.param($scope.data)).then(function(myReponseData) {

            console.log('result');
            console.log(myReponseData);

            $scope.buyResponse = myReponseData.data;
            $scope.status = myReponseData.data.status;

        });
    }

    $scope.getProducts = function () {

        console.log('getProducts');

        $http.get('/api/bitcoin/buybitcoins').then(function(myReponseData) {

            console.log(myReponseData);

            $scope.status = myReponseData.data;

            console.log('result');
            console.log($scope.status);
        });
    }

}]);