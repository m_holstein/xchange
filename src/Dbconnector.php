<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 05.11.2017
 * Time: 11:32
 */

class Dbconnector
{

    private $dbConnection = null;
    private $dbConfig = null;

    public function __construct(DbConfig $dbConfig)
    {
        $this->dbConfig = $dbConfig;
    }

    public function connectDb (){

        $this->dbConnection = new mysqli($this->dbConfig->dbHost, $this->dbConfig->dbUser, $this->dbConfig->dbUserPW, $this->dbConfig->dbName);

        if ($this->dbConnection->connect_error) {
            die("Connection failed: " . $this->dbConnection->connect_error);
        }
    }

    public function getDbConnection(){

        if(is_object($this->dbConnection)){

            return $this->dbConnection;
        }

        return false;
    }

    public function getEffectedRows(){

        $result = $this->dbConnection->query('SELECT ROW_COUNT() AS count');

        $return = $this->processResult($result);

        if (isset($return[0]['count'])){

            return $return[0]['count'];
        }

        return false;
    }

    public function runQuery($sql){

        $return = false;

        $result = $this->dbConnection->query($sql);

        return $this->processResult($result);
    }

    public function runStatement($stmt, $prosessingResult = true){

        $result = $stmt->execute();

        if ($prosessingResult){

            $result = $stmt->get_result();

            $processedResult = $this->processResult($result);

            $stmt ->close();

            return $processedResult;
        } else {

            return $result;
        }
//        if($this->dbConnection->errno == 0){
//
////        $stmt->store_result();
//
//        } else {
//            print "Error: ".$this->dbConnection->error;
//        }

    }

    private function processResult($queryResult){

        $return = array();

        if($this->dbConnection->errno == 0){

            if ($queryResult){

                while ($row = $queryResult->fetch_assoc()){

                    $return[] = $row;
                }
            } else {

                return false;
            }

        } else {

            print "Error: ".$this->dbConnection->error;

            return false;
        }

        return $return;
    }
}