<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 08.11.2017
 * Time: 15:52
 */

namespace Api;


class UserWallet extends \BaseClass
{
    private $coinApi = null;
    private $queries = null;

    public function __construct(\Entity\Api\UserWallet $queries, \Dbconnector $dbconnector)
    {
        $this->queries = $queries;

        parent::__construct($dbconnector);

        $this->coinApi = getBitCoinClient();
    }

    public function setBitcoinApi(\Cryptos\Bitcoin $bitcoinApi)
    {
        $this->coinApi = $bitcoinApi;
    }

    function sendBitcoins($params)
    {
        $userName = $_SESSION['user']['userName'];
        $userId = $_SESSION['user']['userId'];

        $msg = '';

        $tranferUid = uniqid();

        $sendToAddress = trim(filter_var($params['receiveraddress'], FILTER_DEFAULT));
        $amount = trim(filter_var($params['amountsend'], FILTER_DEFAULT));
        $comment = trim(filter_var($params['receivernote'], FILTER_DEFAULT));

        $this->insertNewWithdrawal($userId, $sendToAddress, $amount, $comment, 'Start send', $tranferUid);

        $this->coinApi->sendFromTo($userName, $sendToAddress, $amount, $comment);

        $result = $this->coinApi->response;

        if (is_null($result['result'])) {

            $msg = $result['error']['message'];
        } else {

            $msg = $result['result'];
        }

        $this->insertNewWithdrawal($userId, $sendToAddress, $amount, $msg, 'End send', $tranferUid);

        print $msg;
    }

    public function insertNewWithdrawal($userId, $sendToAddress, $amount, $status, $comment, $tranferUid)
    {
        $userWalletsId = $_SESSION['user']['walletAddresses']['btc']['id'];

        $stmt = $this->queries->insertNewWithdrawal($userId, $userWalletsId, $sendToAddress, $amount, $status, $comment, $tranferUid);

        $result = $this->db->runStatement($stmt, false);

        $rows = $this->db->getEffectedRows();

        return json_encode($result);
    }

    public function getBalance( $params ){

        $currency = filter_var($params['currency'], FILTER_DEFAULT);

        $userName = $_SESSION['user']['userName'];

        $result = $this->coinApi->getbalance($userName);

        return $result;
    }

    public function getReceivedByAddress($userName, $btcAddress = null){

        $received = '';

        if($btcAddress == null){
            $btcAddress = json_decode($this->getWalletAddress($userName));

            if (isset($btcAddress[0])){

                $btcAddress = $btcAddress[0];
            }
        }

        $received = $this->coinApi->getreceivedbyaddress($btcAddress);

        return $received;
    }

    public function getWalletAddress($userName)
    {
//        $userName = 'maxmuster4';

        $result = $this->coinApi->getaddressesbyaccount($userName);

        if(!isset($result[0])){

            $result = json_decode($this->generateNewBitCoinAddress($userName));

            if (strlen($result) > 0){

                $this->storeNewBitCoinAddress($result);
            }
            return false;
        }

        return json_encode($result);
    }

    public function generateNewBitCoinAddress($userName)
    {
        $result = $address = $this->coinApi->getnewaddress($userName);

        return json_encode($result);
    }

    private function storeNewBitCoinAddress($walletAddress){

        $userId = $_SESSION['user']['userId'];

        $stmt = $this->queries->storeNewBitCoinAddress($userId, $walletAddress, 1);

        $result = $this->db->runStatement($stmt);

        return json_encode($result);
    }
}