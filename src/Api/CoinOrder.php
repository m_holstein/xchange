<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 09.11.2017
 * Time: 08:28
 */

namespace Api;


use Dbconnector;

class CoinOrder extends \BaseClass
{
    private $queries = null;
    private $marketApi = null;
    private $userId = null;

    private $currentCryptoPrice = null;
    private $currentDeposit = null;

    private $coinAmount = null;

    private $neededDeposit = null;

    public function __construct(\Entity\Api\CoinOrder $queries, $dbConnector, $userId)
    {
        $this->queries = $queries;

        $this->userId = $userId;

        parent::__construct($dbConnector);
    }

    function checkDepositForBuy(){

        $isDepositOk = false;

        $this->neededDeposit = round( $this->coinAmount * $this->currentCryptoPrice);

        if (($this->currentDeposit > 0 ) && ($this->neededDeposit > 0)){

            if($this->neededDeposit < $this->currentDeposit){

                print "Deposit OK: ".$this->neededDeposit;

                $isDepositOk = true;

            } else {

                print "Deposit to low";
            }
        }

        return $isDepositOk;
    }

    /**
     * @param null $marketApi
     */
    public function setMarketApi($marketApi)
    {
        $this->marketApi = $marketApi;
    }

    public function withdrawDeposit( $currencyId, $message){

        $stmt = $this->queries->withdrawDeposit($this->userId, $this->neededDeposit, $currencyId, $message);

        $result = $this->db->runStatement($stmt);

        $rows = $this->db->getEffectedRows();

        if($rows > 0){

            return true;
        }
        return $result;
    }

    /**
     * @param null $currentCryptoPrice
     */
    public function setCurrentCryptoPrice($currentCryptoPrice)
    {
        $this->currentCryptoPrice = $currentCryptoPrice;
    }

    /**
     * @param null $currentDeposit
     */
    public function setCurrentDeposit($currentDeposit)
    {
        $this->currentDeposit = $currentDeposit;
    }

    /**
     * @param null $coinAmount
     */
    public function setCoinAmount($coinAmount)
    {
        $this->coinAmount = $coinAmount;
    }

    /**
     * @return null
     */
    public function getNeededDeposit()
    {
        return $this->neededDeposit;
    }
}