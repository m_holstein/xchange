<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 14.11.2017
 * Time: 10:35
 */

namespace Api;


class Ledger extends \BaseClass
{
    private $queries = null;

    private $userId = null;

    public function __construct(\Entity\Api\Ledger $queries, \Dbconnector $dbconnector)
    {
        $this->queries = $queries;

        if (!isset( $_SESSION['user']['userId'])){
            die('Not loggedin');
        }
        $this->userId = $_SESSION['user']['userId'];

        parent::__construct($dbconnector);
    }

    public function rechargeDeposit( $depositAmount, $currencyId){

        $stmt = $this->queries->rechargeDeposit($this->userId, $depositAmount, $currencyId);

        $this->db->runStatement($stmt);

        $rows = $this->db->getEffectedRows();

        if ($rows > 0){
            return TRUE;
        } else {

            return FALSE;
        }
    }

    function deposit($params)
    {
        $secretKey = $_SESSION['stripe']['secret_key'];
        $userEmail = $_SESSION['user']['userEmail'];

        \Stripe\Stripe::setApiKey($secretKey);

        $token = $params['stripeToken'];

        $amount = filter_var($params['dataamount'], FILTER_SANITIZE_NUMBER_INT);
        $description = filter_var($params['datadescription'], FILTER_DEFAULT);
        $currency = filter_var($params['currency'], FILTER_DEFAULT);

        $customer = \Stripe\Customer::create(array(
            'email' => $userEmail,
            'source' => $token
        ));

        $charge = \Stripe\Charge::create(array(
            'customer' => $customer->id,
            'amount' => $amount,
            'description' => $description,
            'currency' => $currency
        ));

        $this->rechargeDeposit(number_format(($amount / 100), 2), 1);

        return $charge->__toJSON();
    }
}