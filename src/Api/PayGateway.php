<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 11.12.2017
 * Time: 13:13
 */

namespace Api;


class PayGateway
{
    private $secretKey = null;

    private $ledger = null;

    private $input = null;

    public function __construct(\Entity\Api\PayGateway $queries, \Dbconnector $dbConnector)
    {
        $this->input =json_decode(@file_get_contents("php://input"), true);

        $queries = new \Entity\Api\Ledger($dbConnector);

        $this->ledger =  new \Api\Ledger($queries, $dbConnector );

        $this->secretKey = $_SESSION['stripe']['secret_key'];
    }

    public function __call($name, $arguments)
    {
        $event_json = $this->input;

// Do something with $event_json

        $mailBody = print_r($event_json, true);

        $webHookData = $event_json;

        $receiver = $mailBody['data']['object']['email'];

        sendMail(EMAIL_SUPPORT_RECEIVER, EMAIL_SENDER_NORESPONSE, 'STRIPE Webhook received', $mailBody );

        http_response_code(200); // PHP 5.4 or greater

        $type = $webHookData['type'];

        if($type === 'source.chargeable'){

            $this->chargeSofort($webHookData);
        }

        return;
    }

    function chargeSofort($webHookData){

        $charge = FALSE;

        \Stripe\Stripe::setApiKey($this->secretKey);

        $source = $webHookData['data']['object']['id'];
        $amount = $webHookData['data']['object']['amount'];

        $status = $webHookData['data']['object']['status'];

        if($status == 'chargeable'){

            $charge = \Stripe\Charge::create(array(
                "currency" => "eur",
                "source" => $source,
                "amount" => $amount,
            ));
        }

        $this->Ledger->rechargeDeposit(number_format(($amount / 100), 2), 1);

        return $charge;
    }
}