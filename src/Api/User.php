<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 06.11.2017
 * Time: 18:50
 */

namespace Api;

class User extends \BaseClass
{
    private $queries = null;


    public function __construct(\Entity\Api\User $queries, \Dbconnector $dbconnector)
    {
        $this->queries = $queries;

        parent::__construct($dbconnector);
    }

    public function activateUserAccount($params){

        $activatingUid = filter_var($params['uuid'], FILTER_DEFAULT);

        $stmt = $this->queries->activateUserAccount($activatingUid);

        $this->db->runStatement($stmt);

        $result = $this->db->getEffectedRows();

        if ( $result != 1){

            return 'failed';
        } else{

            return 'success';
        }
    }

    public function checkPassword($password, $oldHash)
    {
        $new = [
            'options' => ['cost' => 11],
            'algo' => PASSWORD_DEFAULT,
            'hash' => null
        ];

        if (true === password_verify($password, $oldHash)) {

            if (true === password_needs_rehash($oldHash, PASSWORD_DEFAULT)) {

                $newHash = password_hash($password, $new['algo'], $new['options']);
                return $newHash;
            } else {

                return true;
            }
        }

        return false;
    }

    public function isUsernameAvailable( $params ){

        $username = $params['username'];

        $stmt = $this->queries->isUsernameAvailable($params);

        $result = $this->db->runStatement($stmt);

        if (isset($result[0])){

            return false;
        }

        return true;
    }

    function login($params)
    {
        $userEmail = filter_var($params['useremail'], FILTER_DEFAULT);
        $userPassword = filter_var($params['userpassword'], FILTER_DEFAULT);

        $userData = $this->loginUser($userEmail, $userPassword);

        if($userData){

            $userId = $userData['userId'];
            $userName = $userData['userName'];
            $oldPwHash = $userData['password'];

            $result = $this->checkPassword($userPassword, $oldPwHash);

            if(is_string($result)){

                $newPwHash = $result; // Just for documentation

                $result = $this->updatePassword($userId, $newPwHash);

                if($result === false) {

                    writeLog('Error update password hash UserId: '.$userId, ' New password hash: '.$result);
                    return false;

                } else {

                    $sessionController = new \SessionController($this->db );
                    $sessionController->setSessionUserArray($userId, $userName, $userEmail);

                    return 'true';
                }

            } elseif ($result){

                $sessionController = new \SessionController($this->db );
                $sessionController->setSessionUserArray($userId, $userName, $userEmail);

                return 'true';
            } else {

                return 'false';
            }
        } else {

            return 'false';
        }

    }

    public function loginUser($userEmail, $userPassword)
    {
        $stmt = $this->queries->loginUser($userEmail);

        $result = $this->db->runStatement($stmt);

        if (isset($result[0])){

            return $result[0];
        }

        return false;
    }

    public function registerNewUser( $params ){
        $userName = filter_var($params['username'], FILTER_DEFAULT);
        $userEmail = filter_var($params['useremail'], FILTER_DEFAULT);
        $userPassword = filter_var($params['userpassword'], FILTER_DEFAULT);

        $password = password_hash($userPassword, PASSWORD_DEFAULT);

        $activatinguid = md5(microtime());

        print $this->addUser($userName, $password, $userEmail, $activatinguid);

        print $mailBody = $this->renderTemplate('userActivationMail.html.twig', array('activation_link' => SITE_BASE_URL.'/activate/'.$activatinguid));

        sendMail($userEmail, 'CryptoCoinLab', 'Please Activate your Account', $mailBody);
    }

    function resetPassword($params){

        $resetToken = filter_var($params['resettoken'], FILTER_DEFAULT);
        $password = filter_var($params['password'], FILTER_DEFAULT);

        $password = password_hash($password, PASSWORD_DEFAULT);

        $stmt = $this->queries->resetPassword($resetToken, $password);

        $result = $this->db->runStatement($stmt, false);

        $rows = $this->db->getEffectedRows();

        if($rows > 0){
            return "Password successfull changed";
        } else {

            return "Password reset failed";
        }
    }

    function sendPwResetMail($params){

        $userEmail = filter_var($params['useremail'], FILTER_DEFAULT);

         $pwResetToken = md5(microtime());

        $tokenIsSet = $this->setPasswordResetToken($userEmail, $pwResetToken);

        if($tokenIsSet){

            $resetLink = SITE_BASE_URL.'/pwreset/'.$pwResetToken;

            $content = array('passwordResetLink' => $resetLink);

            $mailBody = $this->renderTemplate('userPasswordResetMail.html.twig', $content);

            $result = sendMail($userEmail, EMAIL_SENDER_NORESPONSE, 'Password reset link', $mailBody );

            if($result){

                return 'E-Mail successfull send. Please check your E-Mail account.';
            } else {

                return 'Error during sending E-Mail. Please try again later.';
            }
        } else {

            return 'Error during sending E-Mail. Please try again later.';
        }

    }

    public function addUser($userName, $userPassword, $userEmail, $activatingUid)
    {

        $stmt = $this->queries->addUserQuery($userName, $userPassword, $userEmail, $activatingUid);

        $result = $this->db->runStatement($stmt);

        return json_encode($result);
    }

    public function getCurrentDeposit($userId)
    {

        $deposit = false;

        $stmt = $this->queries->getCurrentDeposit($userId);

        $result = $this->db->runStatement($stmt);

        if (isset($result[0])) {

            $deposit = (float)$result[0]['deposit'];
            print json_encode($deposit);
        }

        return $deposit;
    }

    public function getUserData($userEmail)
    {

        $query = $this->queries->getUserDataQuery($userEmail);
        $result = $this->db->runQuery($query);

        print json_encode($result);
    }

    public function setPasswordResetToken($userEmail, $resetToken){

        $stmt = $this->queries->setPasswordResetToken($userEmail, $resetToken);

        $result = $this->db->runStatement($stmt, false);

        $rows = $this->db->getEffectedRows();

        if($rows < 1){

            return false;
        }

        return $result;
    }

    public function updatePassword($userId, $passwordHash){

        $stmt = $this->queries->updatePassword($userId, $passwordHash);

        $result = $this->db->runStatement($stmt, false);

        $rows = $this->db->getEffectedRows();

        if($rows < 1){

            return false;
        }

        return $result;
    }
}