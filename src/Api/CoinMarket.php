<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 18.11.2017
 * Time: 08:36
 */

namespace Api;


class CoinMarket extends \BaseClass
{
    private $client = null;

    private $coinOrderController = null;

    public function __construct(\Entity\Api\CoinMarket $queries, \Dbconnector $dbConnector)
    {
        parent::__construct($dbConnector);

        $this->client = $this->initMarketClient();
    }

    function exchangeToBitCoin($amount)
    {
        $order = new \GDAX\Types\Request\Authenticated\Order();

        $order->setType('market');
        $order->setProductId('BTC-EUR');
        $order->setSide('buy');
        $order->setSize($amount);

        $response = $this->client->placeOrder($order);

        $id = $response->getId();
        $msg = $response->getMessage();

        $success = false;

        writeLog(__METHOD__.':'.$id.' amount: '.$amount);

        if ($id != null){

            writeLog(__METHOD__.':Error: '.$msg);

            return true;
        }

        return FALSE;
    }

    function initMarketClient()
    {
        if (!is_object($_SESSION['market_client'])) {

            $configuration = \Coinbase\Wallet\Configuration::apiKey('47425e2c51ae67c307271bf0219bebca',
                'IEbVfCUQlqDA5FFIzkvd20Ic+CuXl07fyy2TgaPfJc3MyNbPLr1YbEBpGJjVi1xgV2yI/wC53dM+xBuVXI3gRQ==');

            $client = new \GDAX\Clients\AuthenticatedClient(
                COIN_MARKET_API_KEY,
                COIN_MARKET_API_KEY_SECRET,
                COIN_MARKET_API_KEY_PASSPHRASE
            );

            $client->setBaseURL(COIN_MARKET_API_URL);

            $_SESSION['market_client'] = $client;
        }

        return $_SESSION['market_client'];
    }

    function getCurrentBtcPrice()
    {
        $product = (new \GDAX\Types\Request\Market\Product())->setProductId(\GDAX\Utilities\GDAXConstants::PRODUCT_ID_BTC_EUR);

        $productTicker = $this->client->getProductTicker($product);

        $result = $productTicker->toArray();

        return json_encode($result);
    }

    function orderCoins( $currentDeposit, $amount)
    {
        $resultArray = array('success' => FALSE, 'msg' => 'unknown error');

        $userBtcAddress = $_SESSION['user']['walletAddresses']['btc']['address'];

        $currentCryptoPrice = json_decode($this->getCurrentBtcPrice(), true);

        $this->coinOrderController->setCurrentCryptoPrice($currentCryptoPrice['price']);
        $this->coinOrderController->setCurrentDeposit($currentDeposit);
        $this->coinOrderController->setCoinAmount($amount);

        $isDepositOk = $this->coinOrderController->checkDepositForBuy();

        if ($isDepositOk) {

//            return "Start Order";
            if ($this->exchangeToBitCoin($amount)){

                $result = $this->startCoinOrder($userBtcAddress, $amount);
    //            $result = '{"trade_id":5721782,"price":8134.79,"size":0.01,"bid":8134.86,"ask":8134.87,"volume":5097.98154333,"time":"2017-11-27T13:18:36+00:00"}';
                $resultArray = json_decode($result, true);

                if ($resultArray['success']){

                    $msg =  $resultArray['msg'];

                    $StoreResult = $this->coinOrderController->withdrawDeposit(1, $msg);
                }

                return $resultArray;
            } else {

                $resultArray['msg'] = 'Error during exchange';
                return $resultArray;
            }

        } else {

            return "Not enough deposit";
        }

        return;
    }

    function buyBitCoins($params)
    {
        $userId = $_SESSION['user']['userId'];

        $amount = (float)filter_var($params['amountsbuy'], FILTER_DEFAULT);

        $coinOrderQueries = new \Entity\Api\CoinOrder($this->db);
        $coinOrder = new \Api\CoinOrder($coinOrderQueries, $this->db, $userId);

        $this->setCoinOrderController($coinOrder, $userId);

        $currentDeposit = getCurrentDeposit($this->db);

        $result = $this->orderCoins( $currentDeposit, $amount);

        if($result['success']){

            return "Buying Bitcoin amount: ".$amount. " for ".$this->getNeededDeposit()." EUR successfull: ".$result['msg'];
        } else {

            return "Buying Bitcoin amount: ".$amount. " for ".$this->getNeededDeposit()." EUR FAILED! ".$result['msg'];
        }
    }

    /**
     * @param null $coinOrderController
     */
    public function setCoinOrderController($coinOrderController, $userId)
    {
        if (is_object($coinOrderController)){

            $this->coinOrderController = $coinOrderController;
        } else {
            $coinOrderQueries = new \Entity\Api\CoinOrder($this->db);

            $this->coinOrderController = new \Api\CoinOrder($coinOrderQueries, $this->db, $userId);
        }
    }

    public function getNeededDeposit(){

        return $this->coinOrderController->getNeededDeposit();
    }

    private function startCoinOrder($userBtcAddress, $amount)
    {
        $withdrawer = new \GDAX\Types\Request\Authenticated\Withdrawal();

        $withdrawer->setCurrency('BTC');
        $withdrawer->setCryptoAddress($userBtcAddress);
        $withdrawer->setAmount($amount);

        $response = $this->client->withdrawCrypto($withdrawer);

        $id = $response->getId();
        $msg = $response->getMessage();

        $success = false;

        if ($id != null){

            $msg = $id;
            $success = true;
        }

        $result = array('msg' => $msg
                        , 'success' => $success);

        return json_encode($result);
    }
}