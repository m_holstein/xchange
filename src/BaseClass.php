<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 07.11.2017
 * Time: 14:46
 */

class BaseClass
{
    protected $db = null;
    protected $twig = null;

    public function __construct(Dbconnector $dbconnector)
    {
        global $templateOptions;

        $this->db = $dbconnector;

        $templatePath = dirname(__FILE__) . '/../web' . $templateOptions['directory'];

        $this->twig = new Twig_Environment(new Twig_Loader_Filesystem($templatePath));
    }

    /**
     * @param $template filename
     * @param $content array
     * @return string
     */
    public function renderTemplate($template, $content)
    {
        $template = $this->twig->load($template);

        return $template->render($content);
    }
}