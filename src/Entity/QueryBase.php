<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 07.11.2017
 * Time: 09:53
 */

namespace Entity;


class QueryBase
{
    protected $dbConnection = null;

    public function __construct( \Dbconnector $dbConnect)
    {

        $this->dbConnection = $dbConnect->getDbConnection();

        if(false === $this->dbConnection){
            die('DB Connection');
        }
    }
}