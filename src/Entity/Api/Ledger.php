<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 14.11.2017
 * Time: 10:36
 */

namespace Entity\Api;


class Ledger extends \Entity\QueryBase
{

    public function rechargeDeposit($userId, $depositAmount, $currencyId){

        $now = gmdate("Y-m-d\TH:i:s\Z");

        $stmt = $this->dbConnection->prepare("INSERT INTO ledger (id, user_id, deposit, currency_id, timestamp) VALUES (UUID(), ?, ?, ?, ?)");

        if($stmt){

            $stmt->bind_param("ssis", $userId, $depositAmount, $currencyId, $now);

        } else {
            print "Error: ".$this->dbConnection->error;
        }

        return $stmt;
    }

}