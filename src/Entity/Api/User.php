<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 06.11.2017
 * Time: 18:50
 */

namespace Entity\Api;


class User extends \Entity\QueryBase
{
    public function activateUserAccount($activatingUid)
    {
        $stmt = $this->dbConnection->prepare("UPDATE users 
                                              SET isactive = 1
                                              , activatinguid = NULL
                                              , lastchange = NOW() 
                                              WHERE activatinguid = ?");

        $stmt->bind_param("s", $activatingUid);

        return $stmt;
    }

    public function isUsernameAvailable($userName)
    {
        $stmt = $this->dbConnection->prepare("SELECT id AS userId
                                            FROM users
                                            WHERE name = ?");

        $stmt->bind_param("s", $userName);

        return $stmt;
    }

    public function loginUser($userEmail)
    {
        $stmt = $this->dbConnection->prepare("SELECT id AS userId,
                                                      name AS userName,
                                                      password AS password
                                            FROM users
                                            WHERE email = ?
                                                AND isactive = 1");

        $stmt->bind_param("s", $userEmail);

        return $stmt;
    }

    public function getCurrentDeposit($userId)
    {
        $stmt = $this->dbConnection->prepare("SELECT SUM(deposit) - SUM(withdraw) AS deposit 
                FROM ledger 
                WHERE user_id = ?");

        $stmt->bind_param("s", $userId);

        return $stmt;
    }

    public function getUserDataQuery($userEmail)
    {
        $sql = "SELECT id AS userId
                , name AS userName
            FROM users
            WHERE email = '" . $userEmail . "'";

        return $sql;
    }

    public function addUserQuery($userName, $userPassword, $userEmail, $activatingUid)
    {
        $stmt = $this->dbConnection->prepare("INSERT INTO users (id, name, password, email, activatinguid) VALUES (UUID(), ?, ?, ?, ?) ");
        $stmt->bind_param("ssss", $userName, $userPassword, $userEmail, $activatingUid);

        return $stmt;
    }

    public function resetPassword($resetToken, $password)
    {
        $stmt = $this->dbConnection->prepare("UPDATE users SET password = ?
                                                            , lastchange = now()
                                                            , pwresetuid = NULL 
                                                            WHERE pwresetuid = ?");
        $stmt->bind_param("ss", $password, $resetToken);

        return $stmt;
    }

    public function setPasswordResetToken($userEmail, $resetToken)
    {

        $stmt = $this->dbConnection->prepare("UPDATE users SET pwresetuid = ?
                                                            , lastchange = now()
                                                            WHERE email = ?");
        $stmt->bind_param("ss", $resetToken, $userEmail);

        return $stmt;
    }

    public function updatePassword($userId, $passwordHash)
    {
        $stmt = $this->dbConnection->prepare("UPDATE users SET password = ?
                                                            WHERE id = ?");
        $stmt->bind_param("ss", $passwordHash, $userId);

        return $stmt;
    }

}