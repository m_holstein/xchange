<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 08.11.2017
 * Time: 15:35
 */

namespace Entity\Api;


class UserWallet extends \Entity\QueryBase
{

    public function insertNewWithdrawal ($userId, $userWalletsId, $sendToAddress, $amount, $status, $comment, $tranferUid ){

        $stmt = $this->dbConnection->prepare("INSERT INTO withdrawal ( id, users_id, user_wallets_id,  receiver_adress, amount, status, comment, status_timestamp, tranferuid)
                                               VALUES (UUID(),? ,? ,? ,? ,? ,? , now(), ?)");

        if($stmt){

            $stmt->bind_param("sssssss", $userId, $userWalletsId, $sendToAddress, $amount, $status, $comment, $tranferUid);
        } else {
            print "Error: ".$this->dbConnection->error;
        }

        return $stmt;
    }

    public function storeNewBitCoinAddress($userId, $walletAddress, $walletTypesId){

        $stmt = $this->dbConnection->prepare("INSERT INTO user_wallets ( id, users_id, wallet_address,  wallet_types_id, lastchange)
                                               VALUES (UUID(),? ,? ,? , now())");

        if($stmt){

            $stmt->bind_param("ssi", $userId, $walletAddress, $walletTypesId);
        } else {

            print "Error: ".$this->dbConnection->error;
        }

        return $stmt;
    }
}