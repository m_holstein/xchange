<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 14.11.2017
 * Time: 20:27
 */

namespace Entity\Api;


class CoinOrder extends \Entity\QueryBase
{
    public function withdrawDeposit($userId, $withdrawDeposit, $currencyId, $message = ''){

        $stmt = $this->dbConnection->prepare("INSERT INTO ledger (id, user_id, withdraw, currency_id, timestamp, message) VALUES (UUID(), ?, ?, ?, now(), ?)");

        if($stmt){

            $stmt->bind_param("ssis", $userId, $withdrawDeposit, $currencyId, $message);

        } else {
            print "Error: ".$this->dbConnection->error;
        }

        return $stmt;
    }
}