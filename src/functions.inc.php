<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 16.11.2017
 * Time: 08:37
 */

function getBitCoinClient(){

    $user = $_SESSION['bitcoin_api']['wallet_user'];
    $password = $_SESSION['bitcoin_api']['wallet_user_pw'];
    $host = $_SESSION['bitcoin_api']['bitcoind_host'];
    $host_port = $_SESSION['bitcoin_api']['bitcoind_port'];

    return new \Cryptos\Bitcoin($user, $password, $host, $host_port );
}

function getUserWalletInstance($dbConnector){

    $queries = new \Entity\Api\UserWallet($dbConnector);
    $wallet = new \Api\UserWallet($queries, $dbConnector);

    $bitcoinApi = getBitCoinClient();

    $wallet->setBitcoinApi($bitcoinApi);

    return $wallet;
}

function isUserLoggedin()
{

    if (!isset($_SESSION['user'])) {

        writeLog('No logged in');
        return false;
    }

    return true;
}

function sendMail($receiver, $sender, $subject, $msg){

    $headers = "From: ".$sender. "\r\n";

    $result = mail($receiver, $subject, $msg, $headers);

    if ($result === 1){
        return true;
    } else {

        return $result;
    }
}

function writeLog( $msg ){
    $date = date('Y-m-d h:i:s');

    $msg = PHP_EOL."[".$date."] ".$msg;

    error_log($msg, 3, LOGFILE_TARGET);
}