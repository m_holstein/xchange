<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 15.11.2017
 * Time: 12:24
 */

class SessionController extends BaseClass
{
    function logout(){

        session_destroy();
        $_SESSION = array();

        return 'true';
    }

    function setSessionUserArray( $userId, $userName, $userEmail ){

        global $dbConnector;

        $wallet = getUserWalletInstance($dbConnector);

        $btcAddress = json_decode($wallet->getWalletAddress($userName));

        if(isset($btcAddress[0])){

            $btcAddress = $btcAddress[0];
        } else {
            $btcAddress = 'Failed to get BTC Address';
        }

        $_SESSION['user'] = array(
            'userName' => $userName,
            'userEmail' => $userEmail,
            'userId' => $userId,
            'walletAddresses' => array(
                                        'btc' => array ('address' => $btcAddress,
                                                            'id' => 'b0b7f184-c60f-11e7-bc3f-a81e84311332')
                                        )
        );
    }


}