-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 04. Dez 2017 um 20:31
-- Server-Version: 5.7.18-0ubuntu0.16.04.1
-- PHP-Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `xchangev2`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) NOT NULL,
  `currency_code` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ledger`
--

CREATE TABLE `ledger` (
  `id` char(36) NOT NULL,
  `user_id` char(36) DEFAULT NULL,
  `withdraw` decimal(12,2) DEFAULT '0.00',
  `deposit` decimal(12,2) DEFAULT '0.00',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `currency_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `ledger`
--

INSERT INTO `ledger` (`id`, `user_id`, `withdraw`, `deposit`, `timestamp`, `currency_id`) VALUES
('', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '1100.00', '2017-11-14 16:41:41', 1),
('05b938a9-cab7-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '99.00', '2017-11-16 10:15:08', 1),
('14347113-cc6a-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.66', '0.00', '2017-11-18 14:09:23', 1),
('1c24b82f-c939-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '1206.00', '2017-11-14 16:41:41', 1),
('1ea2e7cd-cc86-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '50.00', '2017-11-18 17:30:06', 1),
('2366c875-c94d-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '0.00', '2017-11-14 16:41:41', 1),
('3bfcae4d-c934-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '1204.00', '2017-11-14 16:41:41', 1),
('48dfc8c4-d377-11e7-bc3f-a81e84311332', '69cb56c2-cb85-11e7-bc3f-a81e84311332', '0.81', '0.00', '2017-11-27 13:31:33', 1),
('64336b2f-c939-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '1206.00', '2017-11-14 16:41:41', 1),
('671bd627-ca20-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '44.00', '2017-11-15 16:16:57', 1),
('6fa67353-c980-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.57', '0.00', '2017-11-14 21:11:52', 1),
('777a407f-c97e-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '0.00', '2017-11-14 20:57:46', 1),
('86bb9310-c934-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '1205.00', '2017-11-14 16:41:41', 1),
('8f8f6c5b-cab6-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '40.00', '2017-11-16 10:11:49', 1),
('a52ff334-c94d-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '555.01', '2017-11-14 16:41:41', 1),
('ae64d621-cc4b-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.66', '0.00', '2017-11-18 10:31:47', 1),
('afc3261a-c939-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '1206.00', '2017-11-14 16:41:41', 1),
('b0233d70-c97e-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '0.00', '2017-11-14 20:59:21', 1),
('c1ac147e-d0f5-11e7-bc3f-a81e84311332', '69cb56c2-cb85-11e7-bc3f-a81e84311332', '0.00', '320.03', '2017-11-24 08:59:19', 1),
('cb0b9e8d-c939-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '12.00', '2017-11-14 16:41:41', 1),
('f251eb44-cc47-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.66', '0.00', '2017-11-18 10:05:03', 1),
('f4b053a1-c939-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '0.00', '12.34', '2017-11-14 16:41:41', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `isactive` tinyint(1) DEFAULT '0',
  `pwresetuid` char(36) DEFAULT NULL,
  `activatinguid` char(36) DEFAULT NULL,
  `lastchange` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `isactive`, `pwresetuid`, `activatinguid`, `lastchange`) VALUES
('db42504f-c611-11e7-bc3f-a81e84311332', 'mholstein', '$2y$11$V3oQLA/FAr30gU5x6ClABewMP86boIpI4huMzXv5HrVd5rcsM8ZEm', 'm.holstein@posteo.de', 1, NULL, NULL, '2017-11-15 09:58:17'),
('2', 'ewrw', 'opopÃ¼po', '', 0, 'd648a91e6754f4acc85929fd87c5c07d', NULL, '2017-11-28 19:19:11'),
('3', 'uo8o67i', 'gtrhtjzjhv', 'uzutjjj@djfkdjg.com', 0, NULL, NULL, '2017-11-15 09:58:17'),
('69cb56c2-cb85-11e7-bc3f-a81e84311332', 'maxmuster', '$2y$11$GEUbqJKO07X9WV5FT34G0.IKGSbtcyjlKoGoY3dPTy96tnHkXSV5e', 'mark@cvanb02.gallien.local', 1, NULL, NULL, '2017-11-29 21:35:25'),
('8d34e5c3-d46c-11e7-bc3f-a81e84311332', 'fgdfgfgfs', 'efefewe', 'mh@holstein-web.de', 0, NULL, '4e9e4d9bad66875f15f4b5c186d70ca3', '2017-11-28 18:47:14'),
('514e8d95-d913-11e7-bc3f-a81e84311332', 'hansmuster', '$2y$10$MXYePNpCpDbyW9DHX2YLo.LMGsJ6rtBT0CCqQaxXYTjoxGEP5cI.q', 'hmuster@cvanb02', 1, NULL, NULL, '2017-12-04 16:51:13');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_wallets`
--

CREATE TABLE `user_wallets` (
  `id` char(36) NOT NULL,
  `users_id` char(36) NOT NULL,
  `wallet_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `wallet_types_id` int(10) UNSIGNED NOT NULL,
  `lastchange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user_wallets`
--

INSERT INTO `user_wallets` (`id`, `users_id`, `wallet_address`, `wallet_types_id`, `lastchange`) VALUES
('b0b7f184-c60f-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '1KpH77P9z4Agfpg5L2M2ANqBWsoLeewCAb', 1, '2017-11-15 15:41:28'),
('ed804610-cc55-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '"', 1, '2017-11-18 11:45:08'),
('58ab8654-cc56-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '"1NCnYYmo7xtGmwbq9Y3L3Wh32MmvNMeriV"', 1, '2017-11-18 11:48:08'),
('31565c6a-cc62-11e7-bc3f-a81e84311332', 'db42504f-c611-11e7-bc3f-a81e84311332', '1KGc8Lwahj3viipRfxWYCKMFrsSduURdQU', 1, '2017-11-18 13:12:56');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wallet_types`
--

CREATE TABLE `wallet_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `wallet_type` varchar(10) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `wallet_types`
--

INSERT INTO `wallet_types` (`id`, `wallet_type`) VALUES
(0, 'BTC');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `withdrawal`
--

CREATE TABLE `withdrawal` (
  `id` char(36) NOT NULL,
  `users_id` char(36) NOT NULL,
  `user_wallets_id` char(36) NOT NULL,
  `receiver_adress` varchar(255) NOT NULL,
  `amount` decimal(8,8) UNSIGNED NOT NULL,
  `status` varchar(100) NOT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  `status_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tranferuid` char(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `withdrawal`
--

INSERT INTO `withdrawal` (`id`, `users_id`, `user_wallets_id`, `receiver_adress`, `amount`, `status`, `comment`, `status_timestamp`, `tranferuid`) VALUES
('14242647-d924-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00124000', 'Test7', 'Start send', '2017-12-04 18:51:03', NULL),
('15284aaa-d924-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00124000', '272517d0d3b7caca27b597ca463caedca32653617b5660c5c423328c286f2e7c', 'End send', '2017-12-04 18:51:05', NULL),
('2899547b-d923-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00000000', 'Test6', 'Start send', '2017-12-04 18:44:28', NULL),
('29c56aa3-d923-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00000000', 'ad9df7eb01c330a0f6b46f17f13aa5afeff167eec805ec7db005febf5a574931', 'End send', '2017-12-04 18:44:30', NULL),
('37b22c23-d923-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00123000', 'Test6', 'Start send', '2017-12-04 18:44:53', NULL),
('38f426de-d923-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00123000', '2c90365af63f3b4ebb9fff61152ff1191ca834e88098c6a850dc8b570c250744', 'End send', '2017-12-04 18:44:56', NULL),
('96ae6a72-d922-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00000000', 'Test5', 'Start send', '2017-12-04 18:40:23', NULL),
('a206cb81-d926-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00124000', 'Test7', 'Start send', '2017-12-04 19:09:20', '5a259d56c7f1b'),
('a2af3d83-d922-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00000000', 'e9440d62dba684a8ccba8887341cac1b063fd5134108d896eb82ab62231f29c2', 'End send', '2017-12-04 18:40:43', NULL),
('a636881a-d926-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00124000', 'a0fad1755c3eb41a82154a2eddec3f5c4bcdc884486b3c836fcf893d36fe40d6', 'End send', '2017-12-04 19:09:27', '5a259d56c7f1b'),
('efec05c7-d920-11e7-bc3f-a81e84311332', '69cb56c2-cb85-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mt92fJEjKHKnj4sAwu91Bw6TSZrECKF5KU', '0.00000000', 'Test 4', 'Start send', '2017-12-04 18:28:34', NULL),
('f130cc96-d927-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00321000', 'Send to Mark', 'Start send', '2017-12-04 19:18:43', '5a259f9325ab0'),
('f13c713d-d927-11e7-bc3f-a81e84311332', '514e8d95-d913-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mqyqciqSxq5zvsiZsMg1igH5nEhvPRsxCk', '0.00321000', 'e482ff333b122470898258ec2db70cfe9fc9fb0c21a1de7441b6688bbf50e94f', 'End send', '2017-12-04 19:18:43', '5a259f9325ab0'),
('f6f71db2-d920-11e7-bc3f-a81e84311332', '69cb56c2-cb85-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mt92fJEjKHKnj4sAwu91Bw6TSZrECKF5KU', '0.00000000', 'dd012031b2134eb42b24a56039ed48e41de25fceff41f4637238d946455aa6e4', 'End send', '2017-12-04 18:28:46', NULL),
('fc097ea6-d920-11e7-bc3f-a81e84311332', '69cb56c2-cb85-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mt92fJEjKHKnj4sAwu91Bw6TSZrECKF5KU', '0.00000000', 'Test 4', 'Start send', '2017-12-04 18:28:54', NULL),
('ff00c412-d920-11e7-bc3f-a81e84311332', '69cb56c2-cb85-11e7-bc3f-a81e84311332', 'b0b7f184-c60f-11e7-bc3f-a81e84311332', 'mt92fJEjKHKnj4sAwu91Bw6TSZrECKF5KU', '0.00000000', '0a0d80df0cd4772abc958ce072bac0e1ff8eff0196061a5ab673fe1ed43cd847', 'End send', '2017-12-04 18:28:59', NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `currencies_currency_code_uindex` (`currency_code`);

--
-- Indizes für die Tabelle `ledger`
--
ALTER TABLE `ledger`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ledger_id_uindex` (`id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_uindex` (`email`),
  ADD UNIQUE KEY `users_name_uindex` (`name`);

--
-- Indizes für die Tabelle `user_wallets`
--
ALTER TABLE `user_wallets`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `wallet_types`
--
ALTER TABLE `wallet_types`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`),
  ADD KEY `user_wallets_id` (`user_wallets_id`),
  ADD KEY `receiver_adress` (`receiver_adress`),
  ADD KEY `transferuid__index` (`tranferuid`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
