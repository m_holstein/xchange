<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 26.11.2017
 * Time: 13:54
 */
require __DIR__ . '/../../vendor/autoload.php';

include __DIR__ . '/../../config/config.php';
require_once __DIR__ . '/../../config/dbConfig.php';

$dbConfig = new DbConfig();

$dbConnector = new Dbconnector($dbConfig);
$dbConnector->connectDb();

// Retrieve the request's body and parse it as JSON
$input = @file_get_contents("php://input");
$event_json = json_decode($input);

// Do something with $event_json

$mailBody = print_r($input, true);

$receiver = $mailBody['data']['object']['email'];

sendMail($receiver, EMAIL_SENDER_NORESPONSE, 'STRIPE Webhook received', $mailBody );

http_response_code(200); // PHP 5.4 or greater