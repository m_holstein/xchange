<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 05.11.2017
 * Time: 20:16
 */
require __DIR__ . '/../../vendor/autoload.php';

include __DIR__ . '/../../config/config.php';
require_once __DIR__ . '/../../config/dbConfig.php';

$dbConfig = new DbConfig();

$dbConnector = new Dbconnector($dbConfig);
$dbConnector->connectDb();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $method = filter_input(INPUT_POST, 'method');

    if (!is_null($method)){

        if( isAnonymouseMethod( $method) === FALSE ){

            if (!isUserLoggedin()){
                print "Session error! Please login";

                exit;
            }

            if ('sofortPayment' === $method) {

                print sofortPayment($dbConnector);
            }

        } else {

            if ('checkSession' === $method) {

                print checkSession($dbConnector);
            } elseif ('logout' === $method) {

                print logout($dbConnector);
            }
        }

    } else {

        print processingApiCall($dbConnector);
    }

    exit;
}

$request_parts = explode('/', $_GET['url']); // array('users', 'show', 'abc')
$file_type = $_GET['type'];

//var_dump($request_parts);
//var_dump($_GET);

$realm = $request_parts[0];
$function = $request_parts[1];

if (!isUserLoggedin()){
//    print "Session error! Please login";
    exit;
}

$userName = $_SESSION['user']['userName'];

if('charge' === $realm){

    if ('chargeSofort' === $function) {

        print chargeSofort($dbConnector);
    }

} elseif ('bitcoin' === $realm) {

    if ('getWalletInformation' === $function) {

        $bitcoin = getBitCoinClient();

        print json_encode($bitcoin->getbalance($userName));

    } elseif ('getaccount' === $function) {

        runBitcoinFunctionCall($function, $userName);

    } elseif ('getBalance' === $function) {

        $result = getBalance($userName, $dbConnector);
        print $result;

    }elseif ('getBitCoinAddress' === $function) {

        $result = getBitCoinAddress($dbConnector, $userName);
        print $result;
    }elseif ('getReceivedByAdress' === $function) {

        $result = getReceivedByAddress($dbConnector, $userName);
        print json_encode(array('received' => $result));
    }

} elseif ('user' === $realm) {

    $queries = new \Entity\Api\User($dbConnector);
    $user = new \Api\User($queries, $dbConnector);

    if ('data' === $function) {

        $user->getUserData($request_parts[2]);
    } elseif ('getcurrentdeposit' === $function) {

        getCurrentDeposit($dbConnector);
    }
} else {
    print "Unknow realm";
    sendMail('mark@cvanb02', 'CryptoCoinLab@cvanb02', 'Testmail', 'Das ist die Message');

    exit;
}

function isAnonymouseMethod( $method ){

    $anonmymousMethods = array(
        'login',
        'logout',
        'register',
        'acitvateuser',
        'orderPwResetLink',
        'resetPassword',
        'checkSession',
        'getCurrentBtcPrice',
        'WebHook'
    );

    $result = in_array($method, $anonmymousMethods);

    return $result;
}

function checkSession(){

    if(isset($_SESSION['user']['userId'])){

        return 'loggedIn';
    }
}

function getBalance($userName, $dbConnector){

    $wallet = getUserWalletInstance($dbConnector);

    return $wallet->getBalance($userName);
}

function getBitCoinAddress($dbConnector, $userName){

    $wallet = getUserWalletInstance($dbConnector);

    return $wallet->getWalletAddress($userName);
}

function getReceivedByAddress($dbConnector, $userName){

    $wallet = getUserWalletInstance($dbConnector);

    $received = $wallet->getReceivedByAddress($userName);

    return $received;
}

function logout($dbConnector){

    $sessionController = getSessionController($dbConnector);

    return $sessionController->logout();
}

function getSessionController($dbConnector){

    return  new SessionController($dbConnector);
}

function getCurrentDeposit($dbConnector)
{
    $queries = new \Entity\Api\User($dbConnector);
    $user = new \Api\User($queries, $dbConnector);

    $userId = $_SESSION['user']['userId'];

    $deposit = $user->getCurrentDeposit($userId);

    return $deposit;
}

function sofortPayment()
{
    $secretKey = $_SESSION['stripe']['secret_key'];
    $userEmail = $_SESSION['user']['userEmail'];
    $amount = filter_input(INPUT_POST, 'dataamount', FILTER_SANITIZE_NUMBER_INT);
    $currency = filter_input(INPUT_POST, 'currency');

    \Stripe\Stripe::setApiKey($secretKey);

    $response = \Stripe\Source::create(array(
            "type" => "sofort",
            "amount" => $amount,
            "currency" => $currency,
            "sofort" => array("country" => "DE"),
            "redirect" => array("return_url" => SITE_BASE_URL.'/api/charge/chargeSofort'),
            "owner" => array(
                "email" => $userEmail
            )
        )
    );

    return $response->__toJSON();
}

function chargeSofort(){
    print "Thank you for Deposit";
    http_response_code(200); // PHP 5.4 or greater
    return;
}

function processingApiCall($dbConnector){
    /**
     * New Method to call Class Methods via Request URI Parts
     */
    $uri_parts = parse_url($_SERVER["REQUEST_URI"]);

    $call = explode('/', $uri_parts['path']);

    $class = $call[2];
    $method = $call[3];

    if (isAnonymouseMethod($method) === false) {
        if (!isUserLoggedin()) {
            print "Session error! Please login";

            exit;
        }
    }

    $controllerEntity = '\Entity\Api\\'.$class;
    $controllerName =  '\Api\\'.$class;

    $queries = new $controllerEntity($dbConnector);
    $controller = new $controllerName($queries, $dbConnector);

    $result = $controller->$method($_POST);

    return $result;
}

function runBitcoinFunctionCall($function, $userName)
{

    $bitcoin = new \Cryptos\Bitcoin('nouser', 'mypassword');

    return json_encode($bitcoin->$function($userName));
}