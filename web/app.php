<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 05.11.2017
 * Time: 09:24
 */
require __DIR__ . '/../vendor/autoload.php';

include __DIR__.'/../config/config.php';
require_once __DIR__.'/../config/dbConfig.php';

$dbConfig = new DbConfig();

$dbConnector = new Dbconnector($dbConfig);

$dbConnect = $dbConnector->connectDb();

$templatePath = dirname(__FILE__) . $templateOptions['directory'];

$userName = null;

$loader = new Twig_Loader_Filesystem($templatePath);
$twig = new Twig_Environment($loader);


$template = $twig->load('index.html.twig');

if(isset($_SESSION['user']['userName'])){

    $userName = $_SESSION['user']['userName'];
}


echo $template->render(
                        array(
                            'name' => $userName,
                            'siteUrl' => SITE_BASE_URL,
                            'siteName' => SITE_NAME,
                        )
);

